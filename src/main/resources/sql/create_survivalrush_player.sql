CREATE TABLE IF NOT EXISTS `survivalrush_player` (
  `player_id` INT(11) NOT NULL,
  `skeletons_killed` INT(11) NOT NULL DEFAULT 0,
  `blocks_placed` INT(11) NOT NULL DEFAULT 0,
  `iron_mined` INT(11) NOT NULL DEFAULT 0,
  `diamond_mined` INT(11) NOT NULL DEFAULT 0,
  `gold_mined` INT(11) NOT NULL DEFAULT 0,
  `gravel_mined` INT(11) NOT NULL DEFAULT 0,
  `play_time` INT(11) NOT NULL DEFAULT 0,
  `kills` INT(11) NOT NULL DEFAULT 0,
  `deaths` INT(11) NOT NULL DEFAULT 0,
  `coins_earned` DECIMAL(19,4) NOT NULL DEFAULT 0,
  `wins` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_survivalrush_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;