CREATE TABLE IF NOT EXISTS `survivalrush_player_perks` (
  `player_id` INT(11) NOT NULL,
  `iron_pickaxe` TINYINT(1) NOT NULL DEFAULT 0,
  `looting_sword` TINYINT(1) NOT NULL DEFAULT 0,
  `iron_loot_chance` INT(11) NOT NULL DEFAULT 5,
  `skeletons_per_tnt` INT(11) NOT NULL DEFAULT 50,
  `nbr_blocks_respawn` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_survivalrush_player_perks_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;