package com.gmail.val59000mc.survivalrush.callbacks;

import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.spigotutils.*;
import com.gmail.val59000mc.survivalrush.common.Constants;
import com.gmail.val59000mc.survivalrush.items.RushItems;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import com.gmail.val59000mc.survivalrush.players.RushTeam;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RushCallbacks extends DefaultPluginCallbacks{
	
	@Override
	public HCTeam newHCTeam(String name, ChatColor color, Location spawnpoint){
		return new RushTeam(name, color, spawnpoint);
	}
	
	@Override
	public HCPlayer newHCPlayer(Player player){
		return new RushPlayer(player);
	}
	
	@Override
	public HCPlayer newHCPlayer(HCPlayer hcPlayer){
		return new RushPlayer(hcPlayer);
	}
	
	@Override
	public List<HCTeam> createTeams() {
		List<HCTeam> teams = new ArrayList<HCTeam>();
		
		World world = getApi().getWorldConfig().getWorld();
		
		if(getConfig().getInt("teams.number",2) == 4){
			// 4 teams
			teams.add(newHCTeam("Rouge", ChatColor.RED, Parser.parseLocation(world, getConfig().getString("teams.red"))));
			teams.add(newHCTeam("Bleu", ChatColor.BLUE, Parser.parseLocation(world, getConfig().getString("teams.blue"))));
			teams.add(newHCTeam("Jaune", ChatColor.YELLOW, Parser.parseLocation(world, getConfig().getString("teams.yellow"))));
			teams.add(newHCTeam("Vert", ChatColor.GREEN, Parser.parseLocation(world, getConfig().getString("teams.green"))));
		}else{
			// 2 teams
			teams.add(newHCTeam("Rouge", ChatColor.RED, Parser.parseLocation(world, getConfig().getString("teams.red"))));
			teams.add(newHCTeam("Bleu", ChatColor.BLUE, Parser.parseLocation(world, getConfig().getString("teams.blue"))));
		}

		return teams;
	}
	
	@Override
	public WorldConfig configureWorld(WorldManager wm) {
		
		WorldConfig worldCfg = super.configureWorld(wm);
		
		getConfig().set(Constants.LIVE_ENABLED_NO_LIMIT,false);
		getConfig().set(Constants.LIVE_BEFORE_NO_LIMIT, getConfig().getInt("no-limit-mode.before-no-limit", Constants.DEFAULT_BEFORE_NO_LIMIT));
		getConfig().set(Constants.LIVE_BEFORE_END, getConfig().getInt("no-limit-mode.before-end", Constants.DEFAULT_BEFORE_END));
		RushItems.setup(getApi());
		
		// Remove bed craft		
		Iterator<Recipe> it = Bukkit.getServer().recipeIterator();
		while(it.hasNext()){
			Recipe recipe = it.next();
			if(recipe.getResult().getType().equals(Material.BED)){
				it.remove();
				break;
			}
		}

		World world = worldCfg.getWorld();
		
		world.setGameRuleValue("doFireTick", "true");
		
		return worldCfg;
		
	}
	
	@Override
	public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {
		
		List<String> content = new ArrayList<String>();
		
		RushPlayer rushPlayer = (RushPlayer) hcPlayer;

		// Remaining time (if exists)
		if(getApi().is(GameState.PLAYING)){
			if(getConfig().getBoolean(Constants.LIVE_ENABLED_NO_LIMIT,false)){
				int remainingTime = getConfig().getInt(Constants.LIVE_BEFORE_END);
				content.add(
						getStringsApi().get("rush.scoreboard.before-end").toString()+
						" "+
						ChatColor.GREEN+Time.getFormattedTime(remainingTime)
				);
			}else{
				int remainingTime = getConfig().getInt(Constants.LIVE_BEFORE_NO_LIMIT);
				content.add(
						getStringsApi().get("rush.scoreboard.before-no-limit").toString()+
						" "+
						ChatColor.GREEN+Time.getFormattedTime(remainingTime)
				);
			}
		}
		
		// Kills
		content.add(getStringsApi().get("messages.scoreboard.kills-deaths").toString());
		content.add(" "+ChatColor.GREEN+""+hcPlayer.getKills()+ChatColor.WHITE+"/"+ChatColor.GREEN+hcPlayer.getDeaths());

		
		// Skeletons
		if(rushPlayer.is(PlayerState.PLAYING)){
			content.add(getStringsApi().get("rush.scoreboard.skeletons").toString());
			content.add(" "+ChatColor.GREEN+rushPlayer.getSkeletonKilled());
		}
		
		// Coins
		content.add(getStringsApi().get("messages.scoreboard.coins").toString());
		content.add(" "+ChatColor.GREEN+""+hcPlayer.getMoney());
		
		// Teammate
		if(hcPlayer.getTeam() != null){
			content.add(getStringsApi().get("messages.scoreboard.teammates").toString());
			for(HCPlayer teammate : hcPlayer.getTeam().getMembers()){
				if(teammate.isPlaying()){
					content.add(" "+teammate.getColor()+""+teammate.getName());
				}else{
					content.add(" "+ChatColor.GRAY+""+teammate.getName());
				}
			}
		}								
		
		return content;
	}
	
	@Override
	public void assignStuffToPlayer(HCPlayer hcPlayer){
		if(hcPlayer.getStuff() == null && hcPlayer.hasTeam() && hcPlayer.isOnline()){
			
			hcPlayer.setStuff(
				new Stuff.Builder(hcPlayer.getName())
					.addArmorItems(RushItems.getArmor(hcPlayer))
					.addInventoryItems(RushItems.getItems(hcPlayer))
					.build()
			);
		}
	}
	
	@Override
	public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {

		removePlayerDeathDrops(event.getDrops());
		
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
		
	}

	@Override
	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {

		super.handleKillEvent(event, hcKilled, hcKiller);
		
		removePlayerDeathDrops(event.getDrops());
		
		RushPlayer rushKiller = (RushPlayer) hcKiller;
		if(rushKiller.getIronLootChance() >= Randoms.randomInteger(1, 100)){
			event.getDrops().add(RushItems.getIronLoot());
		}
		
	}
	
	private void removePlayerDeathDrops(List<ItemStack> drops){
		Iterator<ItemStack> it = drops.iterator();
		while(it.hasNext()){
			ItemStack drop = it.next();
			switch(drop.getType()){
				case LEATHER_HELMET:
				case LEATHER_CHESTPLATE:
				case LEATHER_LEGGINGS:
				case LEATHER_BOOTS:
				case STONE_PICKAXE:
				case WOOD_AXE:
					if(drop.getEnchantments().size() == 0){
						it.remove();
					}
					break;
				case IRON_PICKAXE:
					it.remove();
					break;
				case WOOD_SWORD:
					if(drop.getEnchantments().size() == 0 || drop.getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS) > 0){
						it.remove();
					}
					break;
				case IRON_SWORD:
					if(drop.getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS) > 0){
						it.remove();
					}
					break;
				default:
					// don't remove
					break;
			}
		}
	}
	
	@Override
	public Location getFirstRespawnLocation(HCPlayer hcPlayer){
		if(hcPlayer.hasTeam()){
			return hcPlayer.getTeam().getSpawnpoint();
		}
		return null;
	}
	
	@Override
	public Location getNextRespawnLocation(HCPlayer hcPlayer) {
		
		RushPlayer rushPlayer = (RushPlayer) hcPlayer;
		if(hcPlayer.hasTeam() && rushPlayer.getBedLocation() != null && rushPlayer.getBedLocation().isBedPresent()){
			return rushPlayer.getBedLocation().getSafeLocation();
		}
		
		return null;
	}
	
	@Override
	public void respawnPlayer(HCPlayer hcPlayer) {

		RushPlayer rushPlayer = (RushPlayer) hcPlayer;
			
		if(hcPlayer.isOnline() && hcPlayer.hasTeam() && rushPlayer.getBedLocation() != null && rushPlayer.getBedLocation().isBedPresent()){
			hcPlayer.getPlayer().setSaturation(20);
			hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
			getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);
			if(hcPlayer.isOnline()){
				Effects.add(hcPlayer.getPlayer(), PotionEffectType.DAMAGE_RESISTANCE, 50, 0);
				if(getApi().getConfig().getBoolean(Constants.LIVE_ENABLED_NO_LIMIT)){
					Effects.addPermanent(hcPlayer.getPlayer(), PotionEffectType.NIGHT_VISION, 0);
				}
			}
		}else{
			getPmApi().eliminatePlayer(hcPlayer);
		}
		
	}
	
	@Override
	public void startPlayer(HCPlayer hcPlayer) {
		super.startPlayer(hcPlayer);
		if(hcPlayer.isOnline()){
			hcPlayer.getPlayer().setGameMode(GameMode.SURVIVAL);
		}
	}

	
	@Override
	public void eliminatedPlayer(HCPlayer hcPlayer){

		if(hcPlayer.isOnline()){
			getStringsApi()
				.get("rush.player-has-been-eliminated")
				.replace("%player%", hcPlayer.getColoredName())
				.sendChatP();
			getSoundApi().play(Sound.WITHER_HURT, 1, 2);
			printDeathMessage(hcPlayer);
		}else{
			getStringsApi()
				.get("messages.player-has-been-eliminated")
				.replace("%player%", hcPlayer.getName())
				.sendChatP();
		}
		
	}
	
	

//    "},{text:"  >>  Squelettes tués : ",bold:true,color:green},{text:"%skeletons%
//    "},{text:"  >>  Blocs placés : ",bold:true,color:green},{text:"%blocksplaced%
//    "},{text:"  >>  Fer miné : ",bold:true,color:green},{text:"%ironmined%
//    "},{text:"  >>  Diamant miné : ",bold:true,color:green},{text:"%diamondmined%
//    "},{text:"  >>  Or miné : ",bold:true,color:green},{text:"%goldmined%
//    "},{text:"  >>  Gravier miné : ",bold:true,color:green},{text:"%gravelmined%
//    "},{text:"  >>  Temps de jeu (min) : ",bold:true,color:green},{text:"%timeplayed%
	
	@Override
	public void printEndMessage(HCPlayer hcPlayer, HCTeam winningTeam) {

		if(hcPlayer.isOnline()){
			
			RushPlayer rushPlayer =(RushPlayer) hcPlayer;

			Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.winning-team")
					.replace("%game%", getApi().getName())
					.replace("%kills%", String.valueOf(hcPlayer.getKills()))
					.replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
					.replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
					.replace("%skeletons%", String.valueOf(rushPlayer.getSkeletonKilled()))
					.replace("%blocksplaced%", String.valueOf(rushPlayer.getBlocksPlaced()))
					.replace("%ironmined%", String.valueOf(rushPlayer.getIronMined()))
					.replace("%diamondmined%", String.valueOf(rushPlayer.getDiamondMined()))
					.replace("%goldmined%", String.valueOf(rushPlayer.getGoldMined()))
					.replace("%gravelmined%", String.valueOf(rushPlayer.getGravelMined()))
					.replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
					.replace("%winner%", (winningTeam == null ? "Match nul" : winningTeam.getColor()+winningTeam.getName()))
					.toString()
			);
				
		}
		
	}
	
	@Override
	public void printDeathMessage(HCPlayer hcPlayer) {
		if(hcPlayer.isOnline()){

			RushPlayer rushPlayer =(RushPlayer) hcPlayer;
			
			Texts.tellraw(hcPlayer.getPlayer(), getStringsApi().get("messages.end.dead")
					.replace("%kills%", String.valueOf(hcPlayer.getKills()))
					.replace("%deaths%", String.valueOf(hcPlayer.getDeaths()))
					.replace("%hardcoins%", String.valueOf(hcPlayer.getMoney()))
					.replace("%skeletons%", String.valueOf(rushPlayer.getSkeletonKilled()))
					.replace("%blocksplaced%", String.valueOf(rushPlayer.getBlocksPlaced()))
					.replace("%ironmined%", String.valueOf(rushPlayer.getIronMined()))
					.replace("%diamondmined%", String.valueOf(rushPlayer.getDiamondMined()))
					.replace("%goldmined%", String.valueOf(rushPlayer.getGoldMined()))
					.replace("%gravelmined%", String.valueOf(rushPlayer.getGravelMined()))
					.replace("%timeplayed%", String.valueOf(hcPlayer.getTimePlayed()))
					.toString()
			);
		}
	}
	
}
