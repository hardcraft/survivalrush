package com.gmail.val59000mc.survivalrush.common;

public class Constants {

	public static final String LIVE_ENABLED_NO_LIMIT = "config.live.no-limit-mode.enable";
	public static final String LIVE_BEFORE_NO_LIMIT = "config.live.no-limit-mode.before-no-limit";
	public static final String LIVE_BEFORE_END = "config.live.no-limit-mode.before-end";
	
	public static final int DEFAULT_BEFORE_NO_LIMIT = 1200;
	public static final int DEFAULT_BEFORE_END = 600;
	
	public static final int SPAWNER_DELAY = 160;
	public static final int SPAWNER_LIMIT = 6;
	public static final int IRON_LOOT_AMOUNT = 3;
}
