package com.gmail.val59000mc.survivalrush.spawners;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class SkeletonSpawner {
	protected List<Location> locations;
	protected int limit;
	private int delay;
	
	private List<ItemStack> helmets = Lists.newArrayList();
	private List<ItemStack> chestplates = Lists.newArrayList();
	private List<ItemStack> leggings = Lists.newArrayList();
	private List<ItemStack> boots = Lists.newArrayList();
	
	private List<Enchantment> helmetsEnchantments = Lists.newArrayList();
	private List<Enchantment> chestplatesEnchantments = Lists.newArrayList();
	private List<Enchantment> leggingsEnchantments = Lists.newArrayList();
	private List<Enchantment> bootsEnchantments = Lists.newArrayList();
	
	public SkeletonSpawner(List<Location> locations, int limit, int delay) {
		super();
		this.locations = locations;
		this.limit = limit;
		this.delay = delay;
		
		// helmets
		this.helmets.add(new ItemStack(Material.LEATHER_HELMET));
		this.helmets.add(new ItemStack(Material.CHAINMAIL_HELMET));
		this.helmets.add(new ItemStack(Material.GOLD_HELMET));
		this.helmets.add(new ItemStack(Material.IRON_HELMET));
		
		
		this.helmetsEnchantments.add(Enchantment.DURABILITY);
		this.helmetsEnchantments.add(Enchantment.OXYGEN);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.helmetsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.helmetsEnchantments.add(Enchantment.THORNS);
		this.helmetsEnchantments.add(Enchantment.WATER_WORKER);
		
		
		// chestplate
		this.chestplates.add(new ItemStack(Material.LEATHER_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.GOLD_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.IRON_CHESTPLATE));
		
		this.chestplatesEnchantments.add(Enchantment.DURABILITY);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.chestplatesEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.chestplatesEnchantments.add(Enchantment.THORNS);
		
		// leggings
		this.leggings.add(new ItemStack(Material.LEATHER_LEGGINGS));
		this.leggings.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		this.leggings.add(new ItemStack(Material.GOLD_LEGGINGS));
		this.leggings.add(new ItemStack(Material.IRON_LEGGINGS));

		this.leggingsEnchantments.add(Enchantment.DURABILITY);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.leggingsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.leggingsEnchantments.add(Enchantment.THORNS);
		
		// boots
		this.boots.add(new ItemStack(Material.LEATHER_BOOTS));
		this.boots.add(new ItemStack(Material.CHAINMAIL_BOOTS));
		this.boots.add(new ItemStack(Material.GOLD_BOOTS));
		this.boots.add(new ItemStack(Material.IRON_BOOTS));

		this.bootsEnchantments.add(Enchantment.DEPTH_STRIDER);
		this.bootsEnchantments.add(Enchantment.DURABILITY);
		this.bootsEnchantments.add(Enchantment.PROTECTION_ENVIRONMENTAL);
		this.bootsEnchantments.add(Enchantment.PROTECTION_EXPLOSIONS);
		this.bootsEnchantments.add(Enchantment.PROTECTION_FALL);
		this.bootsEnchantments.add(Enchantment.PROTECTION_FIRE);
		this.bootsEnchantments.add(Enchantment.PROTECTION_PROJECTILE);
		this.bootsEnchantments.add(Enchantment.THORNS);
	}
	
	public void startSpawnScheduler(HCGameAPI api){
		api.buildTask("spawn skeletons", new HCTask() {
			@Override
			public void run() {
				spawn();
			}
		})
		.withInterval(delay)
		.build()
		.start();
	}
	
	public void startNoLimitMode(){
		this.helmets.clear();
		this.helmets.add(new ItemStack(Material.IRON_HELMET));
		this.helmets.add(new ItemStack(Material.DIAMOND_HELMET));
		
		this.chestplates.clear();
		this.chestplates.add(new ItemStack(Material.IRON_CHESTPLATE));
		this.chestplates.add(new ItemStack(Material.DIAMOND_CHESTPLATE));
		
		this.leggings.clear();
		this.leggings.add(new ItemStack(Material.IRON_LEGGINGS));
		this.leggings.add(new ItemStack(Material.DIAMOND_LEGGINGS));
		
		this.boots.clear();
		this.boots.add(new ItemStack(Material.IRON_BOOTS));
		this.boots.add(new ItemStack(Material.DIAMOND_BOOTS));
	}

	public int getDelay(){
		return delay;
	}
	
	public void spawn(){

		for(Location location : locations){

			int count = 0;
			for(Entity entity : location.getWorld().getNearbyEntities(location, 8, 8, 8)){
				if(entity.getType().equals( EntityType.SKELETON))
					count++;
			}
			
			if(count < limit){
				Entity skeleton = location.getWorld().spawnEntity(location, EntityType.SKELETON);
				customizeMob(skeleton);
			}
			
		}
		
	}



	private void customizeMob(Entity entity) {
		Skeleton skeleton = (Skeleton) entity;
		skeleton.setCustomName("§eJacky");
		skeleton.setCustomNameVisible(true);
		skeleton.setRemoveWhenFarAway(false);
		
		EntityEquipment equipment = skeleton.getEquipment();

		equipment.setHelmet(getRandomArmor(helmets, helmetsEnchantments));
		equipment.setHelmetDropChance(0.18f);

		equipment.setChestplate(getRandomArmor(chestplates, chestplatesEnchantments));
		equipment.setChestplateDropChance(0.18f);

		equipment.setLeggings(getRandomArmor(leggings, leggingsEnchantments));
		equipment.setLeggingsDropChance(0.18f);

		equipment.setBoots(getRandomArmor(boots, bootsEnchantments));
		equipment.setBootsDropChance(0.18f);
		
		skeleton.setHealth(1);
		
	}

	private ItemStack getRandomArmor(List<ItemStack> items, List<Enchantment> enchantments){
		ItemStack item = items.get(Randoms.randomInteger(0, items.size() - 1)).clone();
		item.addUnsafeEnchantment(enchantments.get(Randoms.randomInteger(0, enchantments.size() - 1)), Randoms.randomInteger(1, 3));
		return item;
	}
}
