package com.gmail.val59000mc.survivalrush.players;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.survivalrush.beds.BedLocation;
import org.bukkit.entity.Player;

public class RushPlayer extends HCPlayer{

	private int skeletonKilled;
	private int blocksPlaced;
	private int ironMined;
	private int diamondMined;
	private int goldMined;
	private int gravelMined;
	
	// perks
	private boolean hasIronPickaxe;
	private boolean swordLooting;
	private int ironLootChance;
	private int skeletonsPerTNT;
	private int nbrBlocksRespawn;
	
	private BedLocation bedLocation;
	
	public RushPlayer(Player player) {
		super(player);
		
		this.skeletonKilled = 0;
		this.blocksPlaced = 0;
		this.ironMined = 0;
		this.diamondMined = 0;
		this.goldMined = 0;
		this.gravelMined = 0;
		
		// perks
		this.hasIronPickaxe = false;
		this.swordLooting = false;
		this.ironLootChance = 5;
		this.skeletonsPerTNT = 50;
		this.nbrBlocksRespawn = 0;
		
		this.bedLocation = null;
	}
	
	public RushPlayer(HCPlayer hcPlayer) {
		super(hcPlayer);
		RushPlayer rushPlayer = (RushPlayer) hcPlayer;
		
		this.skeletonKilled = rushPlayer.skeletonKilled;
		this.blocksPlaced = rushPlayer.blocksPlaced;
		this.ironMined = rushPlayer.ironMined;
		this.goldMined = rushPlayer.goldMined;
		this.diamondMined = rushPlayer.diamondMined;
		this.gravelMined = rushPlayer.gravelMined;
		
		// perks
		this.hasIronPickaxe = rushPlayer.hasIronPickaxe;
		this.swordLooting = rushPlayer.swordLooting;
		this.ironLootChance = rushPlayer.ironLootChance;
		this.skeletonsPerTNT = rushPlayer.skeletonsPerTNT;
		this.nbrBlocksRespawn = rushPlayer.nbrBlocksRespawn;
		
		this.bedLocation = rushPlayer.bedLocation;
	}
	
	@Override
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
		RushPlayer rushPlayer = (RushPlayer) lastUpdatedSession;
		this.skeletonKilled -= rushPlayer.skeletonKilled;
		this.blocksPlaced -= rushPlayer.blocksPlaced;
		this.ironMined -= rushPlayer.ironMined;
		this.goldMined -= rushPlayer.goldMined;
		this.diamondMined -= rushPlayer.diamondMined;
		this.gravelMined -= rushPlayer.gravelMined;
	}

	public int getRemainingSkeletonsToKill() {
		int rest = getSkeletonKilled()%getSkeletonsPerTNT();
		return (rest == 0 ? getSkeletonsPerTNT() : rest);
	}

	public int getSkeletonKilled() {
		return skeletonKilled;
	}
	
	public void addSkeletonKilled(){
		this.skeletonKilled++;
	}
	
	public int getBlocksPlaced() {
		return blocksPlaced;
	}
	
	public void addBlockPlaced(){
		this.blocksPlaced++;
	}
	public int getIronMined() {
		return ironMined;
	}
	public void addIronMined() {
		Log.debug("+1 iron mined for "+getName());
		this.ironMined++;
	}
	public int getDiamondMined() {
		return diamondMined;
	}
	public void addDiamondMined() {
		Log.debug("+1 diamond mined for "+getName());
		this.diamondMined++;
	}
	public int getGoldMined() {
		return goldMined;
	}
	public void addGoldMined() {
		Log.debug("+1 gold mined for "+getName());
		this.goldMined++;
	}
	public int getGravelMined() {
		return gravelMined;
	}
	public void addGravelMined() {
		Log.debug("+1 gravel mined for "+getName());
		this.gravelMined++;
	}
	public boolean HasIronPickaxe() {
		return hasIronPickaxe;
	}

	public void setHasIronPickaxe(boolean hasIronPickaxe) {
		this.hasIronPickaxe = hasIronPickaxe;
	}
	
	public boolean hasSwordLooting() {
		return swordLooting;
	}

	public void setHasSwordLooting(boolean swordLooting) {
		this.swordLooting = swordLooting;
	}

	public int getIronLootChance() {
		return ironLootChance;
	}

	public void setIronLootChance(int ironLootChance) {
		this.ironLootChance = ironLootChance;
	}

	public int getSkeletonsPerTNT() {
		return skeletonsPerTNT;
	}

	public void setSkeletonsPerTNT(int skeletonsPerTNT) {
		this.skeletonsPerTNT = skeletonsPerTNT;
	}
	
	public int getNbrBlocksRespawn() {
		return nbrBlocksRespawn;
	}

	public void setNbrBlocksRespawn(int nbrBlocksRespawn) {
		this.nbrBlocksRespawn = nbrBlocksRespawn;
	}
	
	public BedLocation getBedLocation() {
		return bedLocation;
	}
	
	public void setBedLocation(BedLocation bedLocation) {
		this.bedLocation = bedLocation;
	}
	

}
