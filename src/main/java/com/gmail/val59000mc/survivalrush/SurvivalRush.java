package com.gmail.val59000mc.survivalrush;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.gmail.val59000mc.survivalrush.callbacks.RushCallbacks;
import com.gmail.val59000mc.survivalrush.listeners.*;
import com.google.common.collect.Sets;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class SurvivalRush extends JavaPlugin {
	
	public void onEnable(){

		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(), "config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(), "lang.yml"));
		
		HCGameAPI game = new HCGame.Builder("Survival Rush", this, config, lang)
				.withPluginCallbacks(new RushCallbacks())
				.withDefaultListenersAnd(Sets.newHashSet(
						new BedListener(),
						new SkeletonSpawnerListener(),
						new NoLimitModeListener(),
						new BlockListener(),
						new SkeletonDeathListener(),
						new PlayerInvicibleListener(),
						new PlayerInventoryListener(),
						new RushMySQLListener()
				))
				.build();
		game.loadGame();
	}
	public void onDisable(){
	}
}
