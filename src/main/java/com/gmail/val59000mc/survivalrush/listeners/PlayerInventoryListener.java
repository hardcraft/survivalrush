package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInventoryListener extends HCListener{

	@EventHandler
	public void onEntityInteract(PlayerInteractAtEntityEvent e){
		removeItemHeldWithoutPerks(e.getPlayer(),e.getPlayer().getItemInHand());
	}
	
	@EventHandler
	public void onBlockInteract(PlayerInteractEvent e){
		removeItemHeldWithoutPerks(e.getPlayer(),e.getItem());
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player){
			removeItemHeldWithoutPerks((Player) e.getDamager(),((Player) e.getDamager()).getItemInHand());
		}
	}
	
	private void removeItemHeldWithoutPerks(Player player, ItemStack hand){
		if(hand != null){
			switch(hand.getType()){
				case IRON_PICKAXE:
					removePickaxe(player,hand);
					break;
				case WOOD_SWORD:
				case IRON_SWORD:
					removeLooting(player,hand);
					break;
				default:
					break;
			}
		}
	}
	
	private void removeLooting(Player player, ItemStack hand) {

		RushPlayer rushPlayer = (RushPlayer) getPmApi().getHCPlayer(player);
		if(rushPlayer != null 
				&& rushPlayer.isPlaying() 
				&& !rushPlayer.hasSwordLooting() 
				&& hand.getEnchantmentLevel(Enchantment.LOOT_BONUS_MOBS) > 0
				&& hand.hasItemMeta()
				&& hand.getItemMeta().getLore() != null){
			hand.removeEnchantment(Enchantment.LOOT_BONUS_MOBS);
			getStringsApi()
				.get("rush.looting-not-unlocked")
				.sendChatP(rushPlayer);
			getSoundApi().play(rushPlayer, Sound.VILLAGER_NO, 1, 2);
		}
		
	}

	private void removePickaxe(Player player, ItemStack hand) {
		RushPlayer rushPlayer = (RushPlayer) getPmApi().getHCPlayer(player);
		if(rushPlayer != null 
				&& rushPlayer.isPlaying() 
				&& !rushPlayer.HasIronPickaxe() 
				&& hand.hasItemMeta()
				&& hand.getItemMeta().getLore() != null){
			hand.setType(Material.STONE_PICKAXE);
			getStringsApi()
				.get("rush.iron-pickaxe-not-unlocked")
				.sendChatP(rushPlayer);
			getSoundApi().play(rushPlayer, Sound.VILLAGER_NO, 1, 2);
		}
	}
	
}
