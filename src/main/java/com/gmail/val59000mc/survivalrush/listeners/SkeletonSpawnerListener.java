package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterStartEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.survivalrush.common.Constants;
import com.gmail.val59000mc.survivalrush.events.RushStartNoLimitModeEvent;
import com.gmail.val59000mc.survivalrush.spawners.SkeletonSpawner;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.List;

public class SkeletonSpawnerListener extends HCListener{
	
	private SkeletonSpawner spawner;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		
		FileConfiguration cfg = getApi().getConfig();
		
		List<Location> locations = new ArrayList<Location>();
		int delay = cfg.getInt("spawners.delay", Constants.SPAWNER_DELAY);
		int limit = cfg.getInt("spawners.limit", Constants.SPAWNER_LIMIT);
		
		List<String> locationsStr = cfg.getStringList("spawners.locations");
		if(locationsStr == null){
			throw new IllegalArgumentException("Cannot parse skeleton spawners locations !");
		}else{
			World world = getApi().getWorldConfig().getWorld();
			for(String locationStr : locationsStr){
				Location location = Parser.parseLocation(world, locationStr);
				locations.add(location);
			}
			spawner = new SkeletonSpawner(locations,limit,delay);
		}
	}
	
	@EventHandler
	public void afterStart(HCAfterStartEvent e){
		if(spawner != null){
			spawner.startSpawnScheduler(getApi());
		}
	}
	
	@EventHandler
	public void onNoLimitStart(RushStartNoLimitModeEvent e){
		if(spawner != null){
			spawner.startNoLimitMode();
		}
	}

}
