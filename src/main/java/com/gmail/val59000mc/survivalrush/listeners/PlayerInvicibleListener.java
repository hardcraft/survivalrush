package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffectType;

public class PlayerInvicibleListener extends HCListener{

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent e){
		if(e.getEntityType().equals(EntityType.PLAYER)){
			Player player = (Player) e.getEntity();
			if(player.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
				e.setCancelled(true);
			}
		}
	}
}
