package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.survivalrush.items.RushItems;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;

public class SkeletonDeathListener extends HCListener{

	@EventHandler
	public void onKillSkeleton(EntityDeathEvent event){

		if(event.getEntity().getType().equals(EntityType.SKELETON)){
			Skeleton skeleton = (Skeleton) event.getEntity();
			if(skeleton.getKiller() != null){
				RushPlayer rushPlayer = (RushPlayer) getPmApi().getHCPlayer(skeleton.getKiller());
				if(rushPlayer != null && rushPlayer.isPlaying()){
					rushPlayer.addSkeletonKilled();
					
					// extra tnt loot
					if(rushPlayer.getSkeletonKilled() % rushPlayer.getSkeletonsPerTNT() == 0){
						getStringsApi()
							.get("rush.skeleton-tnt-loot")
								.replace("%amount%", String.valueOf(rushPlayer.getSkeletonsPerTNT()))
								.sendChatP(rushPlayer);
						getSoundApi().play(rushPlayer, Sound.LEVEL_UP, 1, 2);
						event.getDrops().add(new ItemStack(Material.TNT));
					}
					
					// iron drop chance
					if(rushPlayer.getIronLootChance() >= Randoms.randomInteger(1, 100)){
						event.getDrops().add(RushItems.getIronLoot());
					}
				}
			}
			
			int bones = 0;
			Iterator<ItemStack> it = event.getDrops().iterator();
			while(it.hasNext()){
				ItemStack drop = it.next();
				if(drop.getType().equals(Material.BONE)){
					bones += drop.getAmount();
					it.remove();					
				}
			}
			if(bones > 0){
				event.getDrops().add(new ItemStack(Material.INK_SACK,3*bones,(short) 15));
			}
		}
	}

	
}
