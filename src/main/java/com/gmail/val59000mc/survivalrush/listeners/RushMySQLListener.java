package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;

public class RushMySQLListener extends HCListener{

	// Queries
	private String createSurvivalRushPlayerSQL;
	private String createSurvivalRushPlayerStatsSQL;
	private String createSurvivalRushPlayerPerksSQL;
	private String insertSurvivalRushPlayerSQL;
	private String insertSurvivalRushPlayerStatsSQL;
	private String insertSurvivalRushPlayerPerksSQL;
	private String updateSurvivalRushPlayerGlobalStatsSQL;
	private String updateSurvivalRushPlayerStatsSQL;
	private String selectSurvivalRushPlayerPerksSQL;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createSurvivalRushPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_survivalrush_player.sql");
			createSurvivalRushPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_survivalrush_player_stats.sql");
			createSurvivalRushPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/create_survivalrush_player_perks.sql");
			insertSurvivalRushPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_survivalrush_player.sql");
			insertSurvivalRushPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_survivalrush_player_stats.sql");
			insertSurvivalRushPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"sql/insert_survivalrush_player_perks.sql");
			updateSurvivalRushPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_survivalrush_player_global_stats.sql");
			updateSurvivalRushPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/update_survivalrush_player_stats.sql");
			selectSurvivalRushPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "sql/select_survivalrush_player_perks.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						
						sql.execute(sql.prepareStatement(createSurvivalRushPlayerSQL));
						
						sql.execute(sql.prepareStatement(createSurvivalRushPlayerStatsSQL));
						
						sql.execute(sql.prepareStatement(createSurvivalRushPlayerPerksSQL));
						
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for Survival Rush stats or perks");
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					String id = String.valueOf(e.getHcPlayer().getId());

					try {
						
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertSurvivalRushPlayerSQL, id));
						
						// Insert ctf player stats if not exists
						sql.execute(sql.prepareStatement(insertSurvivalRushPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
						sql.execute(sql.prepareStatement(insertSurvivalRushPlayerPerksSQL, id));
					
						CachedRowSet perks = sql.query(sql.prepareStatement(selectSurvivalRushPlayerPerksSQL, id));
						perks.first();
						RushPlayer survivalRushPlayer = (RushPlayer) e.getHcPlayer();
						survivalRushPlayer.setHasIronPickaxe(perks.getBoolean("iron_pickaxe"));
						survivalRushPlayer.setHasSwordLooting(perks.getBoolean("looting_sword"));
						survivalRushPlayer.setIronLootChance(perks.getInt("iron_loot_chance"));
						survivalRushPlayer.setSkeletonsPerTNT(perks.getInt("skeletons_per_tnt"));
						survivalRushPlayer.setNbrBlocksRespawn(perks.getInt("nbr_blocks_respawn"));
					} catch (SQLException e1) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			RushPlayer rushPlayer = (RushPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						// Update ctf player global stats
						sql.execute(sql.prepareStatement(updateSurvivalRushPlayerGlobalStatsSQL,
								String.valueOf(rushPlayer.getSkeletonKilled()),
								String.valueOf(rushPlayer.getBlocksPlaced()),
								String.valueOf(rushPlayer.getIronMined()),
								String.valueOf(rushPlayer.getDiamondMined()),
								String.valueOf(rushPlayer.getGoldMined()),
								String.valueOf(rushPlayer.getGravelMined()),
								String.valueOf(rushPlayer.getTimePlayed()),
								String.valueOf(rushPlayer.getKills()),
								String.valueOf(rushPlayer.getDeaths()),
								String.valueOf(rushPlayer.getMoney()),
								String.valueOf(rushPlayer.getWins()),
								String.valueOf(rushPlayer.getId())
						));
						
	
						// Update ctf player stats
						sql.execute(sql.prepareStatement(updateSurvivalRushPlayerStatsSQL,
								String.valueOf(rushPlayer.getSkeletonKilled()),
								String.valueOf(rushPlayer.getBlocksPlaced()),
								String.valueOf(rushPlayer.getIronMined()),
								String.valueOf(rushPlayer.getDiamondMined()),
								String.valueOf(rushPlayer.getGoldMined()),
								String.valueOf(rushPlayer.getGravelMined()),
								String.valueOf(rushPlayer.getTimePlayed()),
								String.valueOf(rushPlayer.getKills()),
								String.valueOf(rushPlayer.getDeaths()),
								String.valueOf(rushPlayer.getMoney()),
								String.valueOf(rushPlayer.getWins()),
								String.valueOf(rushPlayer.getId()),
								sql.getMonth(),
								sql.getYear()
						));
					
					}catch(SQLException e){
						Logger.severe("Couldnt update Survival Rush player stats for player="+rushPlayer.getName()+" uuid="+rushPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}
