package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeEndEvent;
import com.gmail.val59000mc.hcgameslib.events.HCBeforePlayEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.survivalrush.common.Constants;
import com.gmail.val59000mc.survivalrush.events.RushStartNoLimitModeEvent;
import com.gmail.val59000mc.survivalrush.items.RushItems;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.potion.PotionEffectType;

public class NoLimitModeListener extends HCListener{

	@EventHandler
	public void beforePlayGame(HCBeforePlayEvent e){
		startCountdownNoLimit();
	}
	
	public void startCountdownNoLimit(){
		
		final int seconds = getApi().getConfig().getInt(Constants.LIVE_BEFORE_NO_LIMIT);
		
		getApi().buildTask("time before no limit mode", new HCTask() {
			
			private int remainingTime = seconds;
			
			@Override
			public void run() {
				remainingTime--;
				getApi().getConfig().set(Constants.LIVE_BEFORE_NO_LIMIT, remainingTime);
				if(remainingTime > 0 
					&& ( 
						(remainingTime <= 120 && remainingTime%30 == 0)
						|| (remainingTime <= 10)
					)
				){
					getStringsApi()
						.get("rush.no-limit-in")
						.replace("%time%", Time.getFormattedTime(remainingTime))
						.sendActionBar();
					getSoundApi().play(Sound.NOTE_STICKS, 0.8f, 2f);
				}
				getPmApi().updatePlayersScoreboards();
				
			}
		})
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				getApi().getConfig().set(Constants.LIVE_ENABLED_NO_LIMIT, true);
				Bukkit.getPluginManager().callEvent(new RushStartNoLimitModeEvent(getApi()));
			}
			
		})
		.addListener(new HCTaskListener(){
			@EventHandler
			public void beforeEnd(HCBeforeEndEvent e){
				getScheduler().stop();
			}
		})
		.withIterations(seconds-1)
		.build()
		.start();
	}
	

	@EventHandler
	public void onNoLimitStart(RushStartNoLimitModeEvent e){
		getStringsApi()
			.get("rush.title-no-limit")
			.replace("%time%", Time.getFormattedTime(getApi().getConfig().getInt(Constants.LIVE_BEFORE_END)))
			.sendTitle(20, 60, 20);
		getSoundApi()
			.play(Sound.WITHER_HURT, 1, 2);
		
		// replace stuff by no limit stuffs
		for(HCPlayer hcPlayer : getPmApi().getPlayers(null, null)){
			if(hcPlayer.is(PlayerState.PLAYING)){
				hcPlayer.setStuff(
					new Stuff.Builder(hcPlayer.getName())
						.addArmorItems(RushItems.getNoLimitArmor(hcPlayer))
						.addInventoryItems(RushItems.getNoLimitItems(hcPlayer))
						.build()
				);
				getApi().getItemsAPI().giveStuffItemsToPlayer(hcPlayer);
			}
			if(hcPlayer.isOnline()){
				Effects.addPermanent(hcPlayer.getPlayer(), PotionEffectType.NIGHT_VISION, 0);
			}
		}
			
		startCountdownEnd();
	}
	
	public void startCountdownEnd(){
		
		final int seconds = getApi().getConfig().getInt(Constants.LIVE_BEFORE_END);
		
		getApi().buildTask("time before end", new HCTask() {
			
			private int remainingTime = seconds;
			
			@Override
			public void run() {
				remainingTime--;
				getApi().getConfig().set(Constants.LIVE_BEFORE_END, remainingTime);
				if(remainingTime > 0 
					&& ( 
						(remainingTime <= 120 && remainingTime%30 == 0)
						|| (remainingTime <= 10)
					)
				){
					getStringsApi()
						.get("rush.end-in")
						.replace("%time%", Time.getFormattedTime(remainingTime))
						.sendActionBar();
					getSoundApi().play(Sound.NOTE_STICKS, 0.8f, 2f);
				}
				getPmApi().updatePlayersScoreboards();
			}
		})
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				getApi().endGame(null);
			}
			
		})
		.addListener(new HCTaskListener(){
			@EventHandler
			public void beforeEnd(HCBeforeEndEvent e){
				getScheduler().stop();
			}
		})
		.withIterations(seconds-1)
		.build()
		.start();
	}
}
