package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.api.impl.HCMessage;
import com.gmail.val59000mc.hcgameslib.events.HCAfterPlayEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.survivalrush.beds.BedLocation;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.material.Bed;

public class BedListener extends HCListener{

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onEnterBed(final PlayerBedEnterEvent event){
		HCPlayer hcPlayer = getPmApi().getHCPlayer(event.getPlayer());
		
		if(hcPlayer != null && hcPlayer.is(true, PlayerState.PLAYING)){
			RushPlayer rushPlayer = (RushPlayer) hcPlayer;
			Block bed = event.getBed();
			BedLocation newBedLocation = new BedLocation(bed.getLocation(), ((Bed) bed.getState().getData()).getFacing());
			if(isAllowedToUseBed(rushPlayer, newBedLocation)){
				if(rushPlayer.getBedLocation() != null && rushPlayer.getBedLocation().isSimilar(newBedLocation)){
					getStringsApi()
						.get("rush.player.bed-already-set")
						.sendChatP(rushPlayer);
					getSoundApi().play(rushPlayer,Sound.STEP_WOOL, 1, 0.8f);
				}else{
					rushPlayer.setBedLocation(newBedLocation);
					getStringsApi()
						.get("rush.player.bed-set")
						.sendChatP(rushPlayer);
					getSoundApi().play(rushPlayer,Sound.STEP_WOOL, 1, 0.8f);
				}
			}else{
				getStringsApi()
					.get("rush.player.bed-not-set")
					.sendChatP(rushPlayer);
				getSoundApi().play(rushPlayer,Sound.VILLAGER_NO, 1, 2);
			}
			
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onStart(HCAfterPlayEvent event){
		
		
		getApi().buildTask("sleep in bed warning", new HCTask() {

			private HCMessage sleepWarning = getStringsApi().get("rush.sleep-warning");
			
			@Override
			public void run() {
				sleepWarning.sendActionBar(getPmApi().getPlayers(true, PlayerState.PLAYING));				
			}
		})
		.withIterations(15)
		.build()
		.start();
	}
	
	private boolean isAllowedToUseBed(RushPlayer rushPlayer, BedLocation newBedLocation){
		return (rushPlayer.hasTeam() && rushPlayer.getTeam().getSpawnpoint().distanceSquared(newBedLocation.getBedHeadLocation()) < 100);
	}
	
}
