package com.gmail.val59000mc.survivalrush.listeners;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import com.google.common.collect.Sets;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class BlockListener extends HCListener{

	private Map<Material, Set<Byte>> unmodifiableMaterials;

	public BlockListener(){
		this.unmodifiableMaterials = new HashMap<>();
	}

    @EventHandler
    public void afterLoad(HCAfterLoadEvent e){
        List<String> materialStrList = getConfig().getStringList("unmodifiable-materials");
        for(String materialStr : materialStrList){

            try{
                String[] split = materialStr.split(" ");
                Material material = null;
                if(split.length >= 1)
                    material = Material.valueOf(split[0]);


                Byte data = null;
                if(split.length >= 2)
                    data = Byte.valueOf(split[1]);

                addUnmodifiableBlock(material, data);
            }catch(Exception ex){
                Log.warn("Couldn't parse unmodifiable material '"+materialStr+"'");
            }
        }

    }

    private void addUnmodifiableBlock(Material material, Byte data){
        if(unmodifiableMaterials.containsKey(material)){
            if(data != null)
                unmodifiableMaterials.get(material).add(data);
        }else{
            if(data == null)
                unmodifiableMaterials.put(material, Sets.newHashSet());
            else
                unmodifiableMaterials.put(material, Sets.newHashSet(data));
        }

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e){
        RushPlayer rushPlayer = (RushPlayer) getPmApi().getHCPlayer(e.getPlayer());
        if(rushPlayer != null && rushPlayer.isPlaying()){
            handleUnmodifiableBlock(e, rushPlayer);

            if(!e.isCancelled())
                rushPlayer.addBlockPlaced();

        }
    }

	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		HCPlayer tPlayer = getPmApi().getHCPlayer(event.getPlayer());
		
		if(tPlayer == null){
			event.setCancelled(true);
			return;
		}

		handleBreakBlock(event);
		handleUnmodifiableBlock(event,tPlayer);
	}

    @EventHandler(ignoreCancelled = true)
    public void onBlockExplode(EntityExplodeEvent event){
        Iterator<Block> it = event.blockList().iterator();
        while(it.hasNext()){
            Block block = it.next();
            if(isUnmodifiableBlock(block))
                it.remove();
        }
    }

    private void handleUnmodifiableBlock(BlockBreakEvent event, HCPlayer hcPlayer) {
        handleUnmodifiableBlock(event, event.getBlock(), hcPlayer);
    }

    private void handleUnmodifiableBlock(BlockPlaceEvent event, HCPlayer hcPlayer) {
        handleUnmodifiableBlock(event, event.getBlock(), hcPlayer);
    }

    private void handleUnmodifiableBlock(Cancellable event, Block block, HCPlayer hcPlayer) {
        if(!event.isCancelled() && isUnmodifiableBlock(block)){
            warnUnmodifiableBlock(hcPlayer);
            event.setCancelled(true);
        }
    }

    @SuppressWarnings("deprecation")
    private boolean isUnmodifiableBlock(Block block) {
        Material material = block.getType();
        Set<Byte> datas = unmodifiableMaterials.get(material);
        if(datas != null){
            return datas.isEmpty() || datas.contains(block.getData());
        }else{
            return false;
        }
    }

    private void warnUnmodifiableBlock(HCPlayer hcPlayer){
        getStringsApi().get("rush.cannot-modify-block").sendChatP(hcPlayer);
    }

	private void handleBreakBlock(BlockBreakEvent e) {
		Block block = e.getBlock();
		
		Player player = e.getPlayer();
		ItemStack tool = player.getItemInHand();
		
		boolean hasStonePickaxeOrBetter = tool != null && (tool.getType().equals(Material.STONE_PICKAXE) || tool.getType().equals(Material.IRON_PICKAXE) || tool.getType().equals(Material.DIAMOND_PICKAXE));
		boolean hasIronPickaxeOrBetter = tool != null && (tool.getType().equals(Material.IRON_PICKAXE) || tool.getType().equals(Material.DIAMOND_PICKAXE));
		
		RushPlayer rushPlayer = (RushPlayer) getPmApi().getHCPlayer(e.getPlayer());
		if(rushPlayer == null || !rushPlayer.isPlaying()){
			e.setCancelled(true);
			return;
		}
		
		switch(block.getType()){
			case GRAVEL:
				rushPlayer.addGravelMined();
				dropItem(e, Material.SULPHUR, 2);
				break;
			case IRON_ORE:
				if(hasStonePickaxeOrBetter){
					rushPlayer.addIronMined();
					dropItem(e, Material.IRON_INGOT, 2);
				}
				break;
			case GOLD_ORE:
				if(hasIronPickaxeOrBetter){
					rushPlayer.addGoldMined();
					dropItem(e, Material.GOLD_INGOT, 2);
				}
				break;
			case DIAMOND_ORE:
				if(hasIronPickaxeOrBetter){
					rushPlayer.addDiamondMined();
					dropItem(e, Material.DIAMOND, 1);
				}
				break;
			default:
				break;
		}
	}
	
	private void dropItem(BlockBreakEvent event, Material material, int amount){
		Block block = event.getBlock();
		block.getWorld().dropItem(block.getLocation().clone().add(0.5,0.5,0.5), new ItemStack(material,amount));
		block.setType(Material.AIR);
		event.setCancelled(true);
	}
}
