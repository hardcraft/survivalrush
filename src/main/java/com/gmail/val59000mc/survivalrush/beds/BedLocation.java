package com.gmail.val59000mc.survivalrush.beds;

import com.gmail.val59000mc.spigotutils.Locations;
import com.google.common.base.Objects;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;


public class BedLocation {
	private Location bedHeadLocation;
	private BlockFace bedFacing;
	public BedLocation(Location bedHeadLocation, BlockFace bedFacing) {
		super();
		this.bedHeadLocation = bedHeadLocation;
		this.bedFacing = bedFacing;
	}
	
	public Location getBedHeadLocation() {
		return bedHeadLocation;
	}
	
	public boolean isBedPresent(){
		Block bedHeadBlock = bedHeadLocation.getWorld().getBlockAt(bedHeadLocation);
		Block bedFootBlock = bedHeadBlock.getRelative(bedFacing.getOppositeFace());
		
		return (bedHeadBlock.getType().equals(Material.BED_BLOCK) && bedFootBlock.getType().equals(Material.BED_BLOCK));
	}

	public Location getSafeLocation() {
		Block head = bedHeadLocation.getBlock();
		return Locations.getFirstFreeLocationOnTop(head.getLocation().add(0.5, 0, 0.5));
	}
	
	public String toString(){
		return Objects.toStringHelper(this)
				.add("bedHeadLocation", Locations.printLocation(bedHeadLocation))
				.add("bedFacing", bedFacing)
				.toString();
	}

	public boolean isSimilar(BedLocation newBedLocation) {
		return bedHeadLocation.distanceSquared(newBedLocation.getBedHeadLocation()) < 1;
	}
	
	
	
}
