package com.gmail.val59000mc.survivalrush.items;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.spigotutils.Colors;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.gmail.val59000mc.spigotutils.items.LeatherArmorMetaBuilder;
import com.google.common.collect.Lists;
import com.gmail.val59000mc.survivalrush.common.Constants;
import com.gmail.val59000mc.survivalrush.players.RushPlayer;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class RushItems {

	private static HCGameAPI api;
	
	public static void setup(HCGameAPI gameApi){
		api = gameApi;
	}
	
	public static List<ItemStack> getArmor(HCPlayer hcPlayer) {

		Color color = Colors.toColor(hcPlayer.getColor());
		
		return Lists.newArrayList(
				new ItemBuilder(Material.LEATHER_HELMET)
					.buildMeta(LeatherArmorMetaBuilder.class)
					.withColor(color)
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.LEATHER_CHESTPLATE)
					.buildMeta(LeatherArmorMetaBuilder.class)
					.withColor(color)
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.LEATHER_LEGGINGS)
					.buildMeta(LeatherArmorMetaBuilder.class)
					.withColor(color)
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.LEATHER_BOOTS)
					.buildMeta(LeatherArmorMetaBuilder.class)
					.withColor(color)
					.withUnbreakable(true)
					.item()
					.build()
			);
		
	}
	
	
	
	public static List<ItemStack> getItems(HCPlayer hcPlayer) {

		RushPlayer rushPlayer = (RushPlayer) hcPlayer;
				
		ItemStack sword = new ItemBuilder(Material.WOOD_SWORD)
			.buildMeta()
			.withUnbreakable(true)
			.item()
			.build();
		if(rushPlayer.hasSwordLooting()){
			sword.addEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
			ItemMeta meta = sword.getItemMeta();
			meta.setLore(Lists.newArrayList(hcPlayer.getColoredName()));
			sword.setItemMeta(meta);
		}
		
		List<ItemStack> items = Lists.newArrayList(
			sword,
			rushPlayer.HasIronPickaxe() ? 
				new ItemBuilder(Material.IRON_PICKAXE)
					.buildMeta()
					.withLore(hcPlayer.getColoredName())
					.withUnbreakable(true)
					.item()
					.build() 
				: new ItemBuilder(Material.STONE_PICKAXE)
					.buildMeta()
					.withUnbreakable(true)
					.item()
					.build(),
			new ItemBuilder(Material.WOOD_AXE)
				.buildMeta()
				.withUnbreakable(true)
				.item()
				.build(),
			new ItemStack(Material.APPLE,2),
			new ItemStack(Material.COOKED_BEEF, 1)
		);
		
		if(rushPlayer.getNbrBlocksRespawn() > 0){
			items.add(new ItemStack(Material.SANDSTONE, rushPlayer.getNbrBlocksRespawn()));
		}
		
		return items;
		
	}
	
	public static List<ItemStack> getNoLimitArmor(HCPlayer hcPlayer){
		
		return Lists.newArrayList(
				new ItemBuilder(Material.IRON_HELMET)
					.buildMeta()
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.IRON_CHESTPLATE)
					.buildMeta()
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.IRON_LEGGINGS)
					.buildMeta()
					.withUnbreakable(true)
					.item()
					.build(),
				new ItemBuilder(Material.IRON_BOOTS)
					.buildMeta()
					.withUnbreakable(true)
					.item()
					.build()
			);
		
	}
	
	public static List<ItemStack> getNoLimitItems(HCPlayer hcPlayer) {

		RushPlayer rushPlayer = (RushPlayer) hcPlayer;
			
		ItemStack sword = new ItemBuilder(Material.IRON_SWORD)
				.buildMeta()
				.withUnbreakable(true)
				.item()
				.build();
		if(rushPlayer.hasSwordLooting()){
			sword.addEnchantment(Enchantment.LOOT_BONUS_MOBS, 1);
			ItemMeta meta = sword.getItemMeta();
			meta.setLore(Lists.newArrayList(hcPlayer.getColoredName()));
			sword.setItemMeta(meta);
		}
			
		List<ItemStack> items = Lists.newArrayList(
			sword,
			new ItemBuilder(Material.IRON_PICKAXE)
				.buildMeta()
				.withUnbreakable(true)
				.item()
				.build() ,
			new ItemBuilder(Material.IRON_AXE)
				.buildMeta()
				.withUnbreakable(true)
				.item()
				.build(),
			new ItemStack(Material.COOKED_BEEF, 8),
			new ItemStack(Material.SANDSTONE, 20 + rushPlayer.getNbrBlocksRespawn()),
			new ItemStack(Material.TNT),
			new ItemStack(Material.REDSTONE_TORCH_ON),
			new ItemStack(Material.GOLDEN_APPLE)
			
		);
		
		return items;
		
	}



	public static ItemStack getIronLoot() {
		return new ItemStack(Material.IRON_INGOT, Constants.IRON_LOOT_AMOUNT);
	}

}
